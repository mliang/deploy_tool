'use strict';

var restModule = {};

restModule.process = function(req, res, next) {
  switch (req.method) {
    case 'POST':
      return this.processPost(req, res, next);
    case 'GET':
      return this.processGet(req, res, next);
    case 'PUT':
      return this.processPut(req, res, next);
    case 'DELETE':
      return this.processDelete(req, res, next);
  }
};

restModule.processGet = function(req, res, next) {
  var restReq = req.frontierReq;
  var restRes;
  if (req.subModule !== null && req.subModule == "changeStatus") {
    restRes = {
      "status": "SUCCESS",
      "result": null
    };
  } else if (req.subModule !== null && req.subModule == "getList") {
    restRes = {
      "status": "SUCCESS",
      "result": [{
        "changeId": "1",
        "changeName": "变更子任务名称1"
      }, {
        "changeId": "2",
        "changeName": "变更子任务名称2"
      }, {
        "changeId": "3",
        "changeName": "变更子任务名称3"
      }, {
        "changeId": "4",
        "changeName": "变更子任务名称4"
      }, {
        "changeId": "5",
        "changeName": "变更子任务名称5"
      }, {
        "changeId": "6",
        "changeName": "变更子任务名称6"
      }, {
        "changeId": "7",
        "changeName": "变更子任务名称7"
      }, {
        "changeId": "8",
        "changeName": "变更子任务名称8"
      }, {
        "changeId": "9",
        "changeName": "变更子任务名称9"
      }]
    };
  }
  res.restRes = restRes;
  next();
};

restModule.processPost = function(req, res, next) {
  var restReq = req.frontierReq;

  var restRes = {
    "status": "SUCCESS",
    "result": {}
  };

  res.restRes = restRes;
  next();
};

restModule.processPut = function(req, res, next) {
  var restReq = req.frontierReq;

  var restRes = {
    "status": "SUCCESS",
    "result": {}
  };

  res.restRes = restRes;
  next();
};

restModule.processDelete = function(req, res, next) {
  var restReq = req.frontierReq;

  var restRes = {
    "status": "SUCCESS",
    "result": {}
  };

  res.restRes = restRes;
  next();
};

module.exports = restModule;