(function() {
  'use strict';

  angular.module('DeployTool')
    .filter('emptyReplace', function() {
      return function(value, str) {
        str = str ? str : '--';
        return value ? value : str;
      };
    })
    .filter('findList', function() {
      return function(value, params) {
        var p = _.find(params.list, function(o) {
          return o[params.key] == value;
        });

        return p && p[params.value];
      };
    })
    .filter('wrapUp', function() {
      return function(value) {
        value = value ? '(' + value + ')' : '';
        return value;
      };
    })
    .filter('subString', function() {
      return function(str, len) {
        if (str && str.length > len) {
          str = _.truncate(str, { length: len, 'omission': '' });
          str += '...';
        }
        return str;
      };
    })
    .filter('splitStr', function() {
      return function(value, type) {
        if (value && type === 'fileName') {
          value = value.split('/').pop();
        }
        return value;
      };
    })
    .filter('fileSizeUnitSwitch', function() {
      return function(value) {
        if (value < 1024) {
          value = value + 'B';
        } else if (value > 1024 && value < 1024 * 1024) {
          value = (value / 1024).toFixed(2) + 'kB';
        } else {
          value = (value / (1024 * 1024)).toFixed(2) + 'MB';
        }
        return value;
      };
    })
    .filter('timeUnitSwitch', function() {
      return function(value) {
        if (value < 60) {
          value = value + 'sec';
        } else if (value > 60) {
          value = (value / 60).toFixed(2) + 'min';
        }
        return value;
      };
    })
    .filter('timeTransform', function() {
      return function(val) {
        var value = '';
        if (val < 60) {
          value = parseInt(val) + 's';
        } else if (val > 60 && val < 3600) {
          value = parseInt(val / 60) + 'm';
          if (parseInt(val % 60)) { value += parseInt(val % 60) + 's'; }
        } else if (val > 3600 && val < 60 * 60 * 24) {
          value = parseInt(val / 3600) + 'h';
          if (parseInt((val % 3600) / 60)) { value += parseInt((val % 3600) / 60) + 'm'; }
          if (parseInt((val % 3600) % 60)) { value += parseInt((val % 3600) % 60) + 's'; }
        } else {
          value = '超过1天';
        }
        return value;
      };
    })
    .filter('pswToDot', function() {
      return function(value) {
        value = value.split('');
        for (var i = 0; i < value.length; i++) {
          value[i] = '●';
        }
        value = value.join('');
        return value;
      };
    })
    .filter('nowOptionText', function() {
      return function(arr, obj, attr) {
        if (arr.length !== 0) {
          return attr ? _.find(arr, obj)[attr] : _.find(arr, obj);
        } else {
          return '';
        }
      };
    })
    .filter('fireFoxTitleWrap', function() {
      return function(str, len) { //len:38
        var newStr = '';
        if (window.navigator.userAgent.toLowerCase().indexOf('firefox') != -1) {
          if (str) {
            var arr = str.split('');
            arr = _.chunk(arr, len);
            arr.forEach(function(item) {
              newStr = newStr + item.join('') + '\n';
            });
          }
        } else {
          newStr = str;
        }
        return newStr;
      };
    });
})();