(function() {
  'use strict';

  angular.module('DeployTool')
    .directive('convertToNumber', function() {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
          ngModel.$parsers.push(function(val) {
            return val !== null ? parseInt(val, 10) : null;
          });
          ngModel.$formatters.push(function(val) {
            return val !== null ? '' + val : null;
          });
        }
      };
    })
    .directive('onFinishRenderFilters', ['$timeout', function($timeout) {
      return {
        restrict: 'A',
        link: function(scope, element, attr) {
          if (scope.$last === true) {
            $timeout(function() {
              scope.$emit('ngRepeatFinished');
            });
          }
        }
      };
    }])
    .directive('listEmptyMsg', function() {
      return {
        restrict: 'A',
        priority: 2000,
        terminal: true,
        replace: true,
        scope: '',
        template: '<div class="list-empty-msg">抱歉，您查看的内容暂无数据 </div>',
        link: function() {

        }
      };
    })
    .directive("fileLocalRead", [function() {
      var DEFAULT_MAX_BYTES = 100 * 1024 * 1024;

      return {
        scope: {
          fileLocalRead: "=",
          validMaxBytes: "=",
          validFail: '='
        },
        link: function(scope, element, attributes) {
          element.bind('change', function changeFn(changeEvent) {
            var reader = new FileReader();
            var newFile = changeEvent.target.files[0];
            reader.onload = function(loadEvent) {
              scope.$apply(function() {
                scope.fileLocalRead = {
                  'file': newFile,
                  'name': newFile.name
                };
                element.val('');
                element.unbind('change');
                var clone = element.clone();
                element.replaceWith(clone);
                element = clone;
                element.bind('change', changeFn);
              });
            };
            validateSize(newFile.size) && reader.readAsDataURL(newFile);
          })

          function validateSize(fileSize) {
            var validMaxBytesNumber = (typeof scope.validMaxBytes === "number") ? scope.validMaxBytes : DEFAULT_MAX_BYTES;
            var fileMaxBytes = _.max([validMaxBytesNumber, DEFAULT_MAX_BYTES]);

            if (fileSize > fileMaxBytes) {
              typeof scope.validFail === 'function' && scope.validFail();
            }

            return fileSize <= fileMaxBytes;
          }
        }
      };
    }])
    .directive("fileDataRead", [function() {
      return {
        scope: {
          fileDataRead: "="
        },
        link: function(scope, element, attributes) {
          element.bind("change", function(changeEvent) {
            var reader = new FileReader();
            reader.onload = function(loadEvent) {
              scope.$apply(function() {
                scope.fileDataRead = {
                  data: loadEvent.target.result,
                  name: changeEvent.target.files[0].name
                };
              });
            };
            reader.readAsDataURL(changeEvent.target.files[0]);
          });
        }
      };
    }])
    .directive("fileTextRead", [function() {
      return {
        scope: {
          fileTextRead: "="
        },
        link: function(scope, element, attributes) {
          element.bind("change", function(changeEvent) {
            var reader = new FileReader();
            reader.onload = function(loadEvent) {
              scope.$apply(function() {
                scope.fileTextRead = {
                  text: loadEvent.target.result,
                  name: changeEvent.target.files[0].name
                };
              });
              element.val('');
            };
            reader.readAsText(changeEvent.target.files[0]);
          });
        }
      };
    }]);
})();