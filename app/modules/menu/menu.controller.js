(function() {
  'use strict';

  angular.module('DeployTool.menu', [])
    .config(['$stateProvider', MenuConfig])
    .controller('MenuController', [MenuController]);

  function MenuConfig($stateProvider) {
    $stateProvider
      .state('menu', {
        url: '/menu',
        templateUrl: 'modules/menu/menu.html',
        controller: 'MenuController',
        controllerAs: 'vm'
      });
  }

  function MenuController() {
    var vm = this;

    activate();

    //////////////////////

    function activate() {}

  }
})();