(function() {
  'use strict';

  angular.module('DeployTool', [
    'ui.router',
    'DeployTool.menu',
    'DeployTool.tasktrack'
  ]);

  angular.module('DeployTool')
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', AppConfig])
    .controller('RootController', ['$rootScope', '$scope', RootController]);

  function AppConfig($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';

    $urlRouterProvider.otherwise('/menu/tasktrack/list');
  }

  function RootController($rootScope, $scope) {
    $rootScope.$on('global.events', function(evt, eventName, params) {
      $rootScope.$broadcast('events.' + eventName, params);
    });
  }


})();