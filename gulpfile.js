var gulp = require('gulp');
var del = require('del');
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');


gulp.task('package', function() {

  gulp.src('bower_components/jquery-ui/themes/smoothness/images/*.*')
    .pipe(gulp.dest('dist/assets/css/images'));

  gulp.src('app/assets/images/*.*')
    .pipe(gulp.dest('dist/assets/images'));

  gulp.src([
      'app/assets/favicon/iconfont.eot',
      'app/assets/favicon/iconfont.svg',
      'app/assets/favicon/iconfont.ttf',
      'app/assets/favicon/iconfont.woff',
      'bower_components/bootstrap/dist/fonts/*.*'
    ])
    .pipe(gulp.dest('dist/assets/css'));

  gulp.src([
      'bower_components/bootstrap/dist/fonts/*.*'
    ])
    .pipe(gulp.dest('dist/assets/fonts'));

  gulp.src('app/modules/*/*.html')
    .pipe(gulp.dest('dist/modules'));

  gulp.src('app/index.html')
    .pipe(usemin({
      css: [ rev],
      js: [ rev]
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('clean', function(cb) {
  del('dist', cb);
});


gulp.task('default', ['package']);